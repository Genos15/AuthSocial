import { Component, OnInit, Output } from '@angular/core';
import { AuthServiceService } from '../auth-service.service';

@Component({
  selector: 'app-deep',
  templateUrl: './deep.component.html',
  styleUrls: ['./deep.component.css']
})
export class DeepComponent implements OnInit {
  title = "AuthSocial";
  info: any = {};
  public username = "";
  public userpassword = "";
  public response: any;
  public imageplus: any;

  constructor(private _authService: AuthServiceService) { }

  ngOnInit() {
    
  }

  async signinFacebook() {
    this.response = await this._authService.getFacebookInfo();
    console.log(this.response);
    let item:any = this.response.additionalUserInfo.profile;
    let custom = new user(item.email, item.first_name, item.last_name, item.picture.data.url, item.picture.data.height, item.picture.data.width, this.response.credential.accessToken);

    this.imageplus = custom.profile_photo;
  }


  async signinGoogle() {
    this.response = await this._authService.getGoogleInfo();
    console.log(this.response);
    let item:any = this.response.additionalUserInfo.profile;
    let custom = new user(item.email, item.given_name, item.family_name, item.picture, 0, 0, this.response.credential.idToken);

    this.imageplus = custom.profile_photo;
  }
}

class user {
  public email: string;
  public first_name: string;
  public last_name: string;
  public profile_photo: string;
  public photo_height: number;
  public photo_width:number;
  public accessToken: string;

  constructor(_email:string, _first_name:string, _last_name:string, _profile_photo:string, _photo_height:number, _photo_width:number, _access_token: string) {
      this.email = _email;
      this.first_name = _first_name;
      this.last_name = _last_name;
      this.profile_photo = _profile_photo;
      this.photo_height = _photo_height;
      this.photo_width = _photo_width;
      this.accessToken = _access_token;
  }
}
